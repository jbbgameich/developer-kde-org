---
title: Distribute
titleLink: Distribute
subtitle: Get your applications to your users
weight: 30
layout: area
---

<section class="py-2">
  <div class="container text-center">
    <h2 class="mb-3">Release your software</h2>
    <div class="tools">
      <a class="tool" href="https://kdevelop.org">
        <div>
          <h3>Flatpak and Flathub</h3>
          <p>
            Flathub is a centralized repository of Flatpak apps. It provides a single point
            of entry for distributing apps on the most popular Linux distributions.
          </p>
        </div>
        <div>
          <img class="img-fluid" src="/develop/flatpak_logo.png" alt="Flatpak logo" />
        </div>
      </a>
      <a class="tool" href="https://kate-editor.org">
        <div>
          <h3>Snapcraft</h3>
          <p>
            Snapcraft is centralized repository for Snap apps. It provides a single point
            of entry for distributing apps on the most popular Linux distributions.
          </p>
        </div>
        <div>
          <img class="img-fluid" src="/develop/snapcraft.png" alt="Snapcraft logo" />
        </div>
      </a>
    </div>
  </div>
</section>

<!-- <section>
  <div class="container text-center">
    <h2>TODO Windows</h2>
  </div>
</section>

<section>
  <div class="container text-center">
    <h2>TODO macOS</h2>
  </div>
</section>

<section>
  <div class="container text-center">
    <h2>TODO Android</h2>
  </div>
</section> -->

<section>
  <div class="container text-center">
    <h2>KNewStuff</h2>
    <p><a href="https://api.kde.org/frameworks/knewstuff/html/index.html">KNewStuff</a> let your users create and distribute addons for your application via <a href="https://store.kde.org">store.kde.org</a></p>
    <a class="d-block learn-more" href="https://api.kde.org/frameworks/knewstuff/html/index.html">Learn more</a>
    <img src="/develop/knewstuff.png" class="img-fluid" />
  </div>
</section>

