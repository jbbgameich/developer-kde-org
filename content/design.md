---
title: Design
subtitle: Design beautiful, usable applications with KDE
weight: 10
layout: area
---

<section>
  <div class="container text-center">
    <h2>Breeze Icons</h2>
    <p>A collection of modern icons for your applications. Licensed under LGPL.</p>
    <h3 class="mb-4">Collorful icons</h3>
    <div class="mx-auto mb-4">
    <div class="icons-grid">
      <img src="https://kde.org/applications/icons/org.kde.Help.svg" alt="help icon" /> 
      <img src="https://kde.org/applications/icons/org.kde.dolphin.svg" alt="folder icon" /> 
      <img src="https://kde.org/applications/icons/org.kde.kfloppy.svg" alt="floppy icon" /> 
      <img src="https://kde.org/applications/icons/org.kde.kolf.svg" alt="floppy icon" /> 
      <img src="https://kde.org/applications/icons/org.kde.kmousetool.svg" alt="floppy icon" /> 
      <img src="https://kde.org/applications/icons/org.kde.Help.svg" alt="floppy icon" /> 
      <img src="https://kde.org/applications/icons/org.kde.ktimer.svg" alt="floppy icon" /> 
    </div>
    </div>
    <h3 class="mb-4">Monochrome action icons</h3>
    <div class="mx-auto mb-4">
    <div class="icons-grid icons-grid-small">
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
     <i class="icon icon_format-text-code"></i>
    </div>
    </div>
    <div class="text-align"><a href="https://cgit.kde.org/breeze-icons.git/about/" class="learn-more">Learn more</a>
  </div>
</section>

<section>
  <div class="container text-center">
    <h2>Cuttelfish</h2>
    <p>Help you find the right icons for your applications</p>
    <div class="text-center">
      <img class="w-75 img-fluid shadow" src="https://origin.cdn.kde.org/screenshots/cuttlefish/cuttlefish.png" />
    </div>
  </div>
</section>

<section>
  <div class="container text-center">
    <h2>KDE Human Interface Guidelines</h2>
    <p>The KDE Human Interface Guidelines (HIG) offer designers and developers a set of recommendations for producing beautiful, usable, and consistent user interfaces for convergent desktop and mobile applications and workspace widgets. Their aim is to improve the experience for users by making application and widget interfaces more consistent and hence more intuitive and learnable.</p>
    <div class="text-center mt-3 mb-4">
      <a href="https://hig.kde.org" class="learn-more">Learn More</a> 
    </div>
    <a href="https://hig.kde.org"><img class="w-100 img-fluid" src="https://hig.kde.org/_images/HIGDesignVisionFullBleed.png" /></a>
  </div>
</section>
